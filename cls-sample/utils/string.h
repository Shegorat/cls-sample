// ReSharper disable CppNonInlineFunctionDefinitionInHeaderFile
#pragma once

#ifndef H_STRING
#define H_STRING

#pragma warning(disable : 4996)

constexpr auto SPACE_CHARS = " \t\n\r\f\v";

#include <algorithm>
#include <vector>
#include <tuple>
#include "literals.h"

std::tuple<std::string, std::string> split_str(const std::string& s, const std::string& separator) {
	const auto s_pos = s.find(separator);

	// if we didn't find separator
	if (s_pos == std::string::npos)
		return std::make_tuple(s, "");

	return std::make_tuple(s.substr(0, s_pos), s.substr(s_pos + separator.length(), std::string::npos));
}

std::vector<std::string> str_split_n(const std::string& s, const std::string& separator) {
	std::vector<std::string> result;
	auto                     s_index = 0;
	auto                     e_index = 0;

	while ((e_index = s.find(separator, s_index)) < s.size()) {
		result.push_back(s.substr(s_index, e_index - s_index));
		s_index = e_index + separator.size();
	}

	if (s_index < s.size()) {
		result.push_back(s.substr(s_index));
	}
	return result;
}

std::string str_tolower(std::string base) {
	std::for_each(base.begin(), base.end(), [](char& c) {
		c = tolower(c);
	});
	return base;
}

size_t get_multiplier(const std::string& base) {
	const auto m = str_tolower(base);

	if (m == "gb" || m == "g")
		return 1_gb;

	if (m == "mb" || m == "m")
		return 1_mb;

	if (m == "kb" || m == "k")
		return 1_kb;

	return 1;
}

int32_t parse_size(const std::string& base) {
	auto main  = 0;
	char md[8] = {};
	sscanf_s(base.c_str(), "%d%s", &main, md);
	return main * get_multiplier(md);
}

#endif
