#pragma once

inline unsigned long long operator "" _kb(const unsigned long long int n) {
	return n * 1024;
}

inline unsigned long long operator "" _mb(const unsigned long long int n) {
	return n * 1024 * 1024;
}

inline unsigned long long operator "" _gb(const unsigned long long int n) {
	return n * 1024 * 1024 * 1024;
}
