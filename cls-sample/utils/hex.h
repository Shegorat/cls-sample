#pragma once

static unsigned char oct2hex(const unsigned char x) {
	if (x >= '0' && x <= '9')
		return x - '0';

	if (x >= 'A' && x <= 'F')
		return x - 'A' + 10;

	if (x >= 'a' && x <= 'f')
		return x - 'a' + 10;

	return 0;
}

constexpr static char HEX_CHARS[] = "0123456789ABCDEF";

static std::string hex2str(const char* data, const size_t length) {
	std::string s;

	for (auto n = 0u; n < length; n++) {
		const auto a = (data[n] >> 4) & 0xF;
		const auto b = data[n] & 0xF;
		s += HEX_CHARS[a];
		s += HEX_CHARS[b];
	}

	return s;
}

static bool str2hex(std::string str, size_t& result_length, char* buffer, const size_t buffer_size = 0) {
	if (buffer == nullptr)
		return false;

	// remove heaxdecimal prefix
	if (str._Starts_with("0x") || str._Starts_with("0X"))
		str = str.substr(2);

	if ((str.length() % 2) != 0)
		return false;

	auto i = 0u;
	auto x = 0u;
	while (x < str.length()) {
		const auto a = (int)oct2hex(str[x++]);
		const auto b = (int)oct2hex(str[x++]);
		buffer[i++]  = (uint8_t)((a << 4) | b);

		if (buffer_size > 0 && i >= buffer_size)
			break;
	}
	result_length = i;

	return true;
}
