#include "./cls/cls_core.hpp"

constexpr auto TEMP_FOLDER = "coroutine";

class coroutine : public cls_core {
	// params
	int32_t  p_thread_count_ = 1;
	size_t   p_buffer_size_  = 4_mb;
	os::path p_temp_dir{};

	void* buffer_ = nullptr;
protected:
	const char* ini_section() override {
		return "filter";
	}

	void init_commands() override {
		// register parameters for command line
		register_command("t", "1", false);
		register_command("b", "4mb", false);

		// register parameters for CLS.ini
		register_command("ThreadCount", "1", true);
		register_command("BufferSize", "4mb", true);
		register_command("TempDir", "", true);
	}

	bool dispatch_args() override {
		for (auto& [cmd, value] : parameters_) {
			if (cmd == "t" || cmd == "ThreadCount") {
				if (!parse_threads_arg(value, p_thread_count_))
					return false;

				continue;
			}

			if (cmd == "b" || cmd == "BufferSize") {
				if (!parse_mem_arg(value, p_buffer_size_))
					return false;

				continue;
			}

			if (cmd == "TempDir") {
				p_temp_dir = os::path(value);
			}
		}

		return true;
	}

	bool validate_args() override {
		p_thread_count_ = max(p_thread_count_, 1);
		p_buffer_size_  = min(max(p_buffer_size_, 128_kb), 2_gb);

		return error_ == 0;
	}

public:
	coroutine(CLS_CALLBACK* callback, void* instance) : cls_core(callback, instance) {}

	bool init_codec() override {
		buffer_ = malloc(p_buffer_size_);
		cls_assert(buffer_ != nullptr, CLS_ERROR_NOT_ENOUGH_MEMORY);

		return error_ == CLS_OK;
	}

	bool deinit_codec() override {
		if (buffer_ != nullptr) {
			free(buffer_);
			buffer_ = nullptr;
		}

		return error_ == CLS_OK;
	}

	void do_process(bool is_compress) override {
		// main process
		while (!eof()) {
			if (error_ != CLS_OK)
				break;

			const auto r = read(buffer_, p_buffer_size_);
			if (!cls_assert(r >= 0, CLS_ERROR_READ))
				break;

			const auto w = write(buffer_, (size_t)r);
			if (!cls_assert(w >= 0, CLS_ERROR_WRITE))
				break;
		}
	}
};

extern "C" _declspec(dllexport) CLS_MAIN ClsMain;

int ClsMain(const int operation, CLS_CALLBACK callback, void* instance) {
	switch (operation) {
		case CLS_COMPRESS:
		case CLS_DECOMPRESS:
		{
			coroutine cls(callback, instance);
			return cls.run(operation);

		}
		default: return CLS_ERROR_NOT_IMPLEMENTED;
	}
}
