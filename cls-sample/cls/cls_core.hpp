#pragma once

#ifndef _H_CLS_CORE_HEADER_
// ReSharper disable once CppInconsistentNaming
#define _H_CLS_CORE_HEADER_


#include <Windows.h>
#include <atomic>
#include <vector>
#include <thread>

#include "cls.h"
#include "../io/cls_stream.hpp"
#include "../os/path.hpp"
#include "../utils/string.h"
#include "../utils/hex.h"

constexpr int CLS_ERROR_INVALID_ARGUMENTS           = -15;
constexpr int CLS_ERROR_CODEC_INITIALIZATION_FAILED = -16;

class cls_core {
	struct parameter {
		std::string name;
		std::string value;

		parameter(std::string s_name, std::string s_value) : name(std::move(s_name)),
		                                                     value(std::move(s_value)) { }
	};

	struct command {
		std::string cmd;
		std::string def;
		bool        is_ini;

		command(std::string s_cmd, std::string s_def, const bool s_is_ini) : cmd(std::move(s_cmd)),
		                                                                     def(std::move(s_def)),
		                                                                     is_ini(s_is_ini) { }
	};

	void load_ini_parameters() {
		if (!ini_file_.is_exists() || commands_.empty())
			return;

		for (auto& cmd : commands_) {
			if (!cmd.is_ini)
				continue;

			char value[CLS_MAX_PARAMSTR_SIZE] = {};

			if (GetPrivateProfileString(ini_section(), cmd.cmd.c_str(), cmd.def.c_str(), value, sizeof(value),
			                            ini_file_.c_str())) {
				parameters_.emplace_back(cmd.cmd, std::string(value));
			}
		}
	}

	void load_parameters() {
		char cmdline[CLS_MAX_PARAMSTR_SIZE] = {};

		callback_(instance_, CLS_GET_PARAMSTR, cmdline, sizeof(cmdline));

		const auto parameters = str_split_n(std::string(cmdline), ":");

		for (auto& p : parameters) {
			auto [name, value] = split_str(p, "=");
			for (auto& cmd : commands_) {
				if (cmd.is_ini)
					continue;

				if (cmd.cmd == name) {
					parameters_.emplace_back(name, value);
					break;
				}

				// try parse solid command, without separator
				if (name._Starts_with(cmd.cmd) && name.length() > cmd.cmd.length() && value.empty() && !cmd.def.empty()) {
					auto n_value = name.substr(cmd.cmd.size());
					auto n_name  = name.substr(0, cmd.cmd.size());
					parameters_.emplace_back(n_name, n_value);
					break;
				}
			}
		}
	}

	bool parse_args() {
		load_parameters();
		load_ini_parameters();

		return true;
	}

protected:
	void*         instance_    = nullptr;
	CLS_CALLBACK* callback_    = nullptr;
	bool          is_compress_ = false;

	std::atomic<int32_t> error_ = CLS_OK;

	os::path work_dir_;
	os::path ini_file_;

	std::vector<parameter> parameters_;
	std::vector<command>   commands_;
	io::cls_stream         stream_;

	void register_command(const std::string& cmd, const std::string& default_value, bool is_ini) {
		commands_.emplace_back(cmd, default_value, is_ini);
	}

	virtual const char* ini_section() {
		return "";
	}

	virtual void init_commands() { }

	virtual bool dispatch_args() {
		return true;
	}

	virtual bool validate_args() {
		return true;
	}

	virtual bool init_codec() {
		return true;
	}

	virtual bool deinit_codec() {
		return true;
	}

	[[nodiscard]]
	bool eof() const {
		return stream_.eof();
	}

	__int64 read(void* buffer, const size_t size) {
		return stream_.read(buffer, size);
	}

	__int64 write(const void* buffer, const size_t size) {
		return stream_.write(buffer, size);
	}

	// ReSharper disable once IdentifierTypo
	bool checked_write(const void* buffer, const size_t size) {
		// ReSharper disable once CppCStyleCast
		return cls_assert(stream_.write(buffer, size) >= 0, CLS_ERROR_WRITE);
	}

	// ReSharper disable once IdentifierTypo
	bool checked_read(void* buffer, const size_t d_size) {
		// ReSharper disable once CppCStyleCast
		return cls_assert(stream_.read(buffer, d_size) >= 0, CLS_ERROR_READ);
	}

	bool cls_assert(const bool condition, const int32_t err_code) {
		if (!condition)
			set_error(err_code);

		return condition;
	}

	void set_error(const int32_t err_code) {
		error_ = err_code;
	}

	bool parse_int_arg(const std::string& arg, int32_t& result) {

		result = strtol(arg.c_str(), nullptr, 0);

		if (result == LONG_MAX || result == LONG_MIN) {
			set_error(CLS_ERROR_INVALID_ARGUMENTS);
			return false;
		}

		return true;
	}

	bool parse_mem_arg(const std::string& arg, size_t& result) {

		result = parse_size(arg);

		if (result <= 0) {
			set_error(CLS_ERROR_INVALID_ARGUMENTS);
			return false;
		}

		return true;
	}

	bool parse_percent_arg(const std::string& arg, float_t& result) {

		result = 0;

		unsigned pos;
		if (pos = arg.find('p'); pos == std::string::npos)
			return false;

		if (pos == std::string::npos) {
			if (pos = arg.find('%'); pos == std::string::npos)
				return false;
		}

		int32_t k;
		if (const auto c = arg.substr(0, pos); !parse_int_arg(c, k))
			return false;

		result = (float_t)k / 100.0f;
		return true;
	}

	bool parse_threads_arg(const std::string& arg, int32_t& result) {

		result                 = -1;
		const auto max_threads = (int32_t)std::thread::hardware_concurrency();

		if (arg.find('%') != std::string::npos || arg.find('p') != std::string::npos) {

			result = max_threads;

			const auto parts = str_split_n(arg, "-");
			for (auto& n : parts) {
				if (float_t p; parse_percent_arg(n, p))
					result = (int32_t)((float_t)result * p);
				else if (int32_t k; parse_int_arg(n, k))
					result -= k;
			}
		}
		else
			return parse_int_arg(arg, result);

		result = max(1, min(result, max_threads));

		return result > 0;
	}

	size_t parse_hex_arg(const std::string& arg, void* buffer, const size_t buffer_size = 0) {

		if (size_t temp = 0; str2hex(arg, temp, (char*)buffer, buffer_size)) {
			return temp;
		}

		set_error(CLS_ERROR_INVALID_ARGUMENTS);
		return 0;
	}

	void virtual do_process(bool is_compress) = 0;

public:
	cls_core() = default;

	virtual ~cls_core() = default;

	cls_core(CLS_CALLBACK* callback, void* instance) : instance_(instance),
	                                                   callback_(callback),
	                                                   stream_(instance, callback) {
		work_dir_ = os::path(os::path::current_directory());
		ini_file_ = os::path(work_dir_, "cls.ini");
	}

	// copy operator, constructor
	cls_core(const cls_core& other)            = delete;
	cls_core& operator=(const cls_core& other) = delete;

	// move operator, constructor
	cls_core(cls_core&& other)            = delete;
	cls_core& operator=(cls_core&& other) = delete;

	// TODO: Add cls version/codec initializer

	[[nodiscard]]
	int error() const {
		return error_;
	}
	
	[[nodiscard]]
	int run(const int operation) {
		init_commands();

		if (!parse_args())
			return CLS_ERROR_INVALID_ARGUMENTS;

		if (!dispatch_args())
			return CLS_ERROR_INVALID_ARGUMENTS;

		if (!validate_args())
			return CLS_ERROR_INVALID_ARGUMENTS;

		if (!init_codec())
			return CLS_ERROR_GENERAL;

		do_process(is_compress_ = operation == CLS_COMPRESS);

		deinit_codec();

		return error_;
	}
};

#endif
