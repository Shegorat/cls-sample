#pragma once

#ifndef _H_CLS_HEADER_
#define _H_CLS_HEADER_

// CLS version 0.10
#define CLS_API_VERSION 10

// All CLS structures are packed in order to avoid compiler incompatibilities
#pragma pack(push, CLS, 1)

// Type of callback passed to ClsMain (see below)
//   instance           - identifies this particular ClsMain call
//   callback_operation - callback operation requested (CLS_FULL_READ, CLS_GET_PARAMSTR and so on)
//   ptr, n             - generic parameters, whose interpretation depends on particular operation. If more parameters are required, ptr points to the record
//   returns negative error code on failure and for some operations informative positive value
typedef int __cdecl CLS_CALLBACK (void* instance, int callback_operation, void *ptr, int n);

// Type of ClsMain. It's the one and only function that implements all the codec functionality
//   operation - operation requested (CLS_COMPRESS and so on)
//   callback  - function that compressor calls to receive any functionality from the caller (including I/O and compression parameters)
//   instance  - passed to callback to distinguish this particular call
//   returns negative error code on failure and for some operations informative positive value
typedef int __cdecl CLS_MAIN (int operation, CLS_CALLBACK callback, void* instance);

// Function that implements whole codec functionality
extern "C" _declspec(dllexport) CLS_MAIN ClsMain;

// CLS_MAIN Operations
const int CLS_INIT                          = 1;       // Called on codec load in order to get more info about it
const int CLS_DONE                          = 2;       // Called before code unload
const int CLS_COMPRESS                      = 3;       // Request to compress (encode) data using I/O via callbacks
const int CLS_DECOMPRESS                    = 4;       // Request to decompress (decode) data using I/O via callbacks
const int CLS_PREPARE_METHOD                = 5;       // Apply all restrictions to method and then return its new formatted params and info (memreqs/blocksize/threads...)
const int CLS_FLUSH                         = 6;       // Called after batch of operations were performed in order to allow codec to release resources [NOT IMPLEMENTED]

// CLS_CALLBACK operations
const int CLS_FULL_READ                     = 4096;    // Read data into buffer ptr:n. Use CLS_FULL_READ+i to read i'th stream. Retcode: <0 - error, 0 - EOF, >0 - amount of data read
const int CLS_PARTIAL_READ                  = 5120;    // Read data partially (buffer may be not filled entirely even if EOF isn't reached). CLS_PARTIAL_READ+i also works. Retcode: the same
const int CLS_FULL_WRITE                    = 6144;    // Write data from buffer ptr:n. CLS_FULL_WRITE+i also works. Retcode: the same
const int CLS_PARTIAL_WRITE                 = 7168;    // Write data partially (number of bytes written may be less than n)

const int CLS_MALLOC                        = 1;       // Alloc n bytes and make *ptr point to this area
const int CLS_FREE                          = 2;       // Free ptr previously allocated by CLS_MALLOC

const int CLS_GET_PARAMSTR                  = 3;       // Put ASCIIZ parameters string into buffer ptr:n
const int CLS_SET_PARAMSTR                  = 4;       // Set parameter string to ASCIIZ string pointed by ptr

const int CLS_GET_COMPRESSION_THREADS       = 5;       // How many threads for compression, may returns 0, if max threads not specified
const int CLS_SET_COMPRESSION_THREADS       = 6;       // Set cls compression threads, must be less than max threads

const int CLS_GET_DECOMPRESSION_THREADS     = 7;       // How many threads for decompression, may returns 0, if max threads not specified
const int CLS_SET_DECOMPRESSION_THREADS     = 8;       // Set cls decompression threads, must be less than max threads

const int CLS_GET_COMPRESSION_MEMORY        = 9;       // Get max memory limit for compression, may return 0, if limit not specified
const int CLS_SET_COMPRESSION_MEMORY        = 10;      // Set compression memory, must be less than max memory limit

const int CLS_GET_DECOMPRESSION_MEMORY      = 11;      // Get max memory limit for decompression, may return 0, if limit not specified
const int CLS_SET_DECOMPRESSION_MEMORY      = 12;      // Set decompression memory, must be less than max memory limit

const int CLS_PRINT_TEXT                    = 13;      // Print text from `ptr` into stderr  [NOT IMPLEMENTED]

const int CLS_DECOMPRESSOR_VERSION          = 14;      // Codec version required for decompression [NOT IMPLEMENTED]
const int CLS_BLOCK                         = 15;      // Block size (for block-wise compressors like bwt) [NOT IMPLEMENTED]
const int CLS_EXPAND_DATA                   = 16;      // May this compressor expand data (like precomp)? [NOT IMPLEMENTED]


// INIT-stage callback operations
const int CLS_ID                            = 101;     // Codec ID as unique ASCIIZ string (for example, "lzma.7zip.org") [NOT IMPLEMENTED]
const int CLS_VERSION                       = 102;     // Codec version as ASCIIZ string (for example, "1.2.3") [NOT IMPLEMENTED]
const int CLS_THREAD_SAFE                   = 103;     // Is the DLL thread-safe, i.e. allows to run multiple copies of ClsMain() simultaneously? [NOT IMPLEMENTED]

// Error codes
const int CLS_OK                            = 0;       // ALL RIGHT
const int CLS_ERROR_GENERAL                 = -1;      // Unclassified error
const int CLS_ERROR_NOT_IMPLEMENTED         = -2;      // Requested feature isn't supported
const int CLS_ERROR_NOT_ENOUGH_MEMORY       = -3;      // Memory allocation failed
const int CLS_ERROR_READ                    = -4;
const int CLS_ERROR_WRITE                   = -5;
const int CLS_ERROR_ONLY_DECOMPRESS         = -6;      // This DLL supports only decompression
const int CLS_ERROR_INVALID_COMPRESSOR      = -7;      // Invalid compression method parameters
const int CLS_ERROR_BAD_COMPRESSED_DATA     = -8;      // Data can't be decompressed
const int CLS_ERROR_NO_MORE_DATA_REQUIRED   = -9;      // Required part of data was already decompressed
const int CLS_ERROR_OUTBLOCK_TOO_SMALL      = -10;     // Output block size in (de)compressMem is not enough for all output data

// Method parameter types
const int CLS_PARAM_INT                     = -1;      // Integer parameter, like grzip:m1 [NOT IMPLEMENTED]
const int CLS_PARAM_STRING                  = -2;      // String  parameter, like lzma:mf=bt4 [NOT IMPLEMENTED]
const int CLS_PARAM_MEMORY_MB               = -3;      // Memory  parameter, like lzma:d64m, measured in megabytes by default [NOT IMPLEMENTED]

// Various sizes
const int CLS_MAX_PARAMSTR_SIZE             = 256;     // Maximum size of string returned by CLS_GET_PARAMSTR
const int CLS_MAX_ERROR_MSG                 = 256;     // Maximum size of error message


/* to do:
- also there're threading issues (like, application allowing to use up to N
threads) and whether dll is thread-safe or not (if not, it can be secured
by loading multiple instances of dll - might be a useful feature as many
experimental compressors are not really incapsulated).
- codecs need to know how much memory for compression / decompression they are supposed to use.
- some codecs might handle multithreading in a smarter way than splitting streams.

- check versions and backward/forward compatibility
- allow multiple codecs in same dll. this may be solved by ClsMain2, ClsMain3... exported
but this may be not enough for some more complex scenarios
+ some interface methods are required for initialization and model flush
(which are not the same as there might be some precalculation required
only once).
- what about detectors
+ now read may be partial, while write should be full. how about full read / partial write?
- alternative i/o: request outbuf, point to the next inbuf

method:, compression:, and 5-6 options of their choice that would show in GUI
*/

#pragma pack(pop, CLS)

#endif