#pragma once

#ifndef H_PATH
#define H_PATH

#include <Windows.h>

namespace os
{
	class path {
#ifdef UNICODE
		using string = std::wstring;
		using char_t = wchar_t;
		constexpr inline static char_t DRIVE_DELIMITER[] = L":\\";
		constexpr inline static char_t DOT_PATH[]        = L".\\";
		constexpr inline static char_t DELIMITER         = L'\\';
		constexpr inline static char_t DOT               = L'.';
		constexpr inline static char_t UNIX_DELIMITER    = L'/';
#else
		using string = std::string;
		using char_t = char;
		constexpr inline static char_t DRIVE_DELIMITER[] = ":\\";
		constexpr inline static char_t DOT_PATH[]        = ".\\";
		constexpr inline static char_t DELIMITER         = '\\';
		constexpr inline static char_t DOT               = '.';
		constexpr inline static char_t UNIX_DELIMITER    = '/';
#endif

		string path_;

		bool open_handle(WIN32_FIND_DATA* lp_data) const {
			const auto h = FindFirstFile(path_.c_str(), lp_data);

			if (h != INVALID_HANDLE_VALUE) {
				FindClose(h);
				return true;
			}
			return false;
		}

		static bool mkdir(const string& dir, const bool force = false) {
			const size_t delim_pos = dir.find_last_of(DELIMITER);
			const string filename  = delim_pos == string::npos ? dir : dir.substr(delim_pos + 1);
			const string parent    = delim_pos == string::npos ? string() : dir.substr(0, delim_pos);

#ifdef MODULES_DBG
			printf("path:: mkdir '%s' '%s'\n", filename.c_str(), parent.c_str());
#endif

			if (parent.empty())
				return true;

			if (const auto d = path(dir); d.is_exists() && d.is_dir())
				return true;

			if (const auto p = path(parent); p.is_exists() && p.is_dir()) {
				return (!force && (filename.find(DOT) != string::npos)) ||
				       CreateDirectory(dir.c_str(), nullptr);
			}

			return mkdir(parent, force) &&
			       ((!force && (filename.find(DOT) != string::npos)) ||
			        CreateDirectory(dir.c_str(), nullptr));
		}

		void prepare_path(const string& dir, const string& filename, const bool normalize = false) {
			if (filename.empty())
				path_ = dir;
			else if (dir.empty())
				path_ = filename;
			else
				path_ = add_backslash(dir) + filename;

			if (normalize)
				add_full_path();

			normalize_delimiters();
		}

	public:
		path() = default;

		// ReSharper disable once CppNonExplicitConvertingConstructor
		path(const char_t* filename, const bool normalize = false) : path_(filename) {
			if (normalize)
				add_full_path();

			normalize_delimiters();
		}

		// ReSharper disable once CppNonExplicitConvertingConstructor
		explicit path(string filename, const bool normalize = false) : path_(std::move(filename)) {
			if (normalize)
				add_full_path();

			normalize_delimiters();
		}

		path(const char_t* dir, const char_t* filename, const bool normalize = false) {
			prepare_path(dir, filename, normalize);
		}

		path(const string& dir, const char_t* filename, const bool normalize = false) {
			prepare_path(dir, filename, normalize);
		}

		path(const string& dir, const string& filename, const bool normalize = false) {
			prepare_path(dir, filename, normalize);
		}

		explicit path(const path& source, const char_t* filename) {
			path_ = add_backslash(source.path_) + filename;
			normalize_delimiters();
		}

		explicit path(const path& source, const string& filename) {
			path_ = add_backslash(source.path_) + filename;
			normalize_delimiters();
		}

		path(const path& other) = default;

		path(path&& other) noexcept : path_(std::move(other.path_)) {
#ifdef MODULES_DBG
			printf("path(%08x):: move constructor\n", (int)this);
#endif
			normalize_delimiters();
		}

		path& operator=(const path& other) = default;

		path& operator=(path&& other) = default;

		bool operator==(const path& other) const {
			return path_ == other.path_;
		}

		bool operator!=(const path& other) const {
			return path_ != other.path_;
		}

		~path() {
#ifdef MODULES_DBG
			printf("path(%08x):: destructor\n", (int)this);
#endif
		}

		[[nodiscard]]
		bool is_exists() const {
			WIN32_FIND_DATA lp_data;
			return open_handle(&lp_data);
		}

		[[nodiscard]]
		bool is_dir() const {
			WIN32_FIND_DATA lp_data;
			if (open_handle(&lp_data))
				return (lp_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

			return false;
		}

		[[nodiscard]]
		bool is_file() const {
			WIN32_FIND_DATA lp_data;
			if (open_handle(&lp_data))
				return (lp_data.dwFileAttributes & FILE_ATTRIBUTE_NORMAL) != 0;

			return false;
		}

		bool mkdirs(const bool force = false) const {
			return mkdir(path_, force);
		}

		[[nodiscard]]
		string filename() const {
			if (path_.empty())
				return string();

			const auto index = path_.find_last_of(DELIMITER);
			if (index == std::string::npos)
				return path_;

			return path_.substr(index + 1);
		}

		[[nodiscard]]
		string filename_ext() const {
			const auto index = path_.find_last_of(DOT);
			if (index == std::string::npos)
				return string();

			return path_.substr(index + 1);
		}

		[[nodiscard]]
		string directory() const {
			return path_.substr(0, path_.find_last_of(DELIMITER));
		}

		[[nodiscard]]
		string drive() const {
			const auto k = path_.find(DRIVE_DELIMITER);
			if (k == std::string::npos)
				return string();

			return path_.substr(0, k + 2);
		}

		void add_full_path() {
			if (drive().empty())
				path_ = add_backslash(current_directory()) + path_;

			if (path_.find(DOT_PATH) == 0)
				path_ = add_backslash(current_directory()) + path_.substr(2);
		}

		void normalize_delimiters() {
#if defined(WIN32) || defined(__WIN32) || defined(__WIN32__)
			while (true) {
				const auto n = path_.find(UNIX_DELIMITER);
				if (n == string::npos)
					break;
				path_.replace(n, 1, 1, DELIMITER);
			}
#else
			while (true) {
				const auto n = path_.find(UNIX_DELIMITER);
				if (n == string::npos)
					break;
				path_.replace(n, 1, 1, DELIMITER);
			}
#endif
		}

		[[nodiscard]]
		string short_path() const {
			const auto dir = current_directory();
			if (path_.find(dir) == 0)
				return path_.substr(dir.size() + 1);
			return path_;
		}

		[[nodiscard]]
		string chop_ext() const {
			return remove_ext(path_);
		}

		[[nodiscard]]
		string str() const {
			return path_;
		}

		[[nodiscard]]
		const char_t* c_str() const {
			return path_.c_str();
		}

		[[nodiscard]]
		bool empty() const {
			return path_.empty();
		}

		[[nodiscard]]
		bool remove() const {
			if (is_dir())
				return RemoveDirectory(path_.c_str());

			return DeleteFile(path_.c_str());
		}

		[[nodiscard]]
		path parent() const {
			return path(directory());
		}


		// static functions		

		static string current_directory() {
			char_t temp[256];
			GetCurrentDirectory(256, temp);
			return string(temp);
		}

		static string add_backslash(string source) {
			if (source.empty() || source.back() == DELIMITER)
				return source;

			return source + DELIMITER;
		}

		static string remove_backslash(string source) {
			if (source[source.length() - 1] == DELIMITER)
				return source.substr(0, source.length() - 1);

			return source;
		}

		static string remove_ext(string source) {
			const auto pos = source.find_last_of(DOT);
			if (pos == string::npos)
				return source;

			return source.substr(0, pos);
		}
	};
};
#endif
