#pragma once
#ifndef H_CLS_STREAM
#define H_CLS_STREAM

#include "../cls/cls.h"

namespace io
{
	class cls_stream final {
		char    error_[1024]{};
		__int32 err_code_{ 0 };

		void*         instance_;
		CLS_CALLBACK* callback_;
		bool          is_eof_;

		__int64 read_internal(void* buffer, const size_t size) {
			const auto result = callback_(instance_, CLS_FULL_READ, buffer, size);
			is_eof_           = result == 0;

			if (result < 0) {
				err_code_ = result;
				// ReSharper disable once CppCStyleCast
				sprintf_s(error_, "io::cls_stream:: callback (op = CLS_FULL_READ, ptr = %td, size = %zd) return %d",
				        (uintptr_t)buffer, size, result);
			}
			return result;
		}

		__int64 write_internal(const void* buffer, const size_t size) {
			const auto result = callback_(instance_, CLS_FULL_WRITE, (void*)buffer, size);

			if (result < 0) {
				err_code_ = result;
				// ReSharper disable once CppCStyleCast
				sprintf_s(error_, "io::cls_stream:: callback (op = CLS_FULL_WRITE, ptr = %td, size = %zd) return %d",
				        (uintptr_t)buffer, size, result);
			}
			return result == CLS_ERROR_NO_MORE_DATA_REQUIRED ? size : result;
		}

	public:
		cls_stream(void* instance, CLS_CALLBACK* callback) : instance_(instance),
		                                                     callback_(callback),
		                                                     is_eof_(false) { }

		[[nodiscard]]
		bool eof() const {
			return is_eof_;
		}

		void print_error() {
			if (err_code_ != 0)
				printf("%s\n", error_);
		}

		__int64 read(void* buffer, const size_t size) {
			return read_internal(buffer, size);
		}

		template <typename T>
		__int64 read_struct(T* data) {
			return read((void*)data, sizeof(T));
		}

		__int64 write(const void* buffer, const size_t size) {
			return write_internal(buffer, size);
		}

		template <typename T>
		__int64 write_struct(T* data) {
			return write((void*)data, sizeof(T));
		}
	};
}

#endif
